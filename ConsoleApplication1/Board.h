#pragma once
#include <vector>// ����� ������
#include "Cell.h"


class Board
{
public:
	Board();// ����������� (������� ��� ��������)
	void ResetMap();//����������� ������ � ����� ��������
	Cell Cells[8][8];// ������ ������ 8�8 
	void Draw();//����� ����� �� �����
	bool CanEat(int x, int y);//���������, ����� �� ����� ������ ����-��
	std::vector<int> CheckMove(int x, int y);//���� ����� ������ �����
	std::vector<int> Chips(int player);//������� ���� ������� ���������� ���������� ����� ,�������� �� ����� ������
	void Move(int x1, int y1, int x2, int y2);//����� �����
};

